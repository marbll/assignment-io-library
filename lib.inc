%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0
%define STDOUT 1
%define STDIN 0
%define NEW_LINE 0xA
%define TAB 0x9
%define SPACE 0x20
%define UINT8_ASSTRING_MAX_SIZE 128
%define ASCII_DIGIT 0x30


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, STDOUT
    mov rax, SYS_WRITE
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rdi, STDOUT
    mov rax, SYS_WRITE
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, rsp
    mov rax, rdi
    mov rdi, 10
    sub rsp, UINT8_ASSTRING_MAX_SIZE
    dec r10
    mov byte[r10], 0
    .loop:
        dec r10
        xor rdx, rdx
        div rdi
        add rdx, 0x30
        mov byte[r10], dl
        test rax, rax
        jz .print
        jmp .loop
    .print:
        mov rdi, r10
        call print_string
        add rsp, UINT8_ASSTRING_MAX_SIZE
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r10, r10
    xor r11, r11
    xor rcx, rcx
    call string_length
    mov r10, rax
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi
    cmp r10, rax
    jne .not_equal
    .loop:
        mov r10b, byte[rdi + rcx]
        mov r11b, byte[rsi + rcx]
        inc rcx
        test r10, r10
        je .equal
        cmp r10, r11
        je .loop
    .not_equal:
        xor rax, rax
        ret
    .equal:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYS_READ
    mov rdx, 1
    mov rdi, STDIN
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

check_empty_symbol:
    xor rax, rax
    cmp rdi, SPACE
    jz .end
    cmp rdi, TAB
    jz .end
    cmp rdi, NEW_LINE
    jz .end
    mov rax, 1
    .end:
    ret


read_word:
    xor r10, r10
    push rdi
    push rsi
    .loop_empty_symbols:
        call read_char
        test rax, rax
        jz .emergency_pop
        push rax
        mov rdi, rax
        call check_empty_symbol
        test rax, rax
        pop rax
        jz .loop_empty_symbols
    .loop:
        pop rsi
        pop rdi
        cmp r10, rsi
        jge .error_exit
        mov [rdi+r10], rax
        inc r10
        push rdi
        push rsi
        call read_char
        test rax, rax
        je .end
        push rax
        mov rdi, rax
        call check_empty_symbol
        cmp rax, 1
        pop rax
        je .loop
    .end:
        pop rsi
        pop rax
        mov rdx, r10
        ret
    .emergency_pop:
        mov rdx, r10
        pop rsi
        pop rdi
    .error_exit:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    xor r11, r11
    mov r11b, byte [rdi]
    cmp r11, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    .loop:
        mov r10b, byte [rdi+rdx]
        cmp r10, '0'
        jb .end
        cmp r10, '9'
        ja .end
        inc rdx
        sub r10, ASCII_DIGIT
        imul rax, 10
        add rax, r10
        jmp .loop
    .end:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        ja .buffer_overflow
        mov r10b, byte [rdi + rcx]
        mov byte [rsi + rcx], r10b
        test r10, r10
        je .compare
        inc rcx
        jmp .loop
    .compare:
        mov rax, rcx
        ret
    .buffer_overflow:
        mov rax, 0
        ret
